from Common import StateManager
from LoreleiServer.Data.Character import CharacterRaceManager, CharacterClassManager
from LoreleiLib.Packets.CharacterCreation import OptionsRequest, ClassSelectionRequest, \
    RaceSelectionRequest, CharacterCheckRequest, CreationOptionsResponse, CharacterCheckResponse, \
    CharacterFinishRequest, CharacterFinishResponse
from LoreleiServer.StateManagers.Game import GamePlayManager
from LoreleiLib.Character import Character
from LoreleiServer.Data.Equipment import EquipmentManager
from LoreleiServer.Database.Character import saveCharacter, loadCharacter


class CharacterCreationManager(StateManager):

    def __init__(self, chat_connection):
        super(CharacterCreationManager, self).__init__(chat_connection)
        self.character = Character(chat_connection.username, None, None)
        self.equipmentManager = EquipmentManager()
        self.characterRaceManager = CharacterRaceManager()
        self.characterClassManager = CharacterClassManager()
        self.handle_start()

    def sendUpdate(self):
        self.connection.sendLine(CharacterCheckResponse(self.character))

    def classSelected(self, packet):
        # type: (ClassSelectionRequest) -> None
        self.character.character_class = packet.characterClass
        for slot, item in packet.characterClass.starterEquipment.iteritems():
           self.character.equipment.equip(item, slot)
        self.character.buildStats()
        self.sendUpdate()

    def raceSelected(self, packet):
        # type: (RaceSelectionRequest) -> None
        self.character.changeRace(packet.characterRace)
        self.character.buildStats()
        self.sendUpdate()

    def handle_data(self, data):
        if isinstance(data, OptionsRequest):
            self.connection.sendLine(CreationOptionsResponse(self.characterRaceManager.Races, self.characterClassManager.Classes))
            self.sendUpdate()
        elif isinstance(data, ClassSelectionRequest):
            self.classSelected(data)
        elif isinstance(data, RaceSelectionRequest):
            self.raceSelected(data)
        elif isinstance(data, CharacterFinishRequest):
            saveCharacter(self.character)
            character = loadCharacter(self.connection.username)
            self.connection.sendLine(CharacterFinishResponse(True, "Character Created!"))
            return GamePlayManager(self.connection, character)