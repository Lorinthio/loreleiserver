from twisted.protocols.basic import LineReceiver


class StateManager(object):

    def __init__(self, chat_connection):
        # type: (LineReceiver) -> None
        self.connection = chat_connection

    def handle_start(self):
        pass

    def handleLogout(self):
        pass

    def handle_data(self, data):
        raise NotImplementedError