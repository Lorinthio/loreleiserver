from LoreleiServer.GameServer.GameServer import GameServer
from LoreleiServer.Data.World.Room import RoomManager
from LoreleiLib.Map import MapTile
from LoreleiLib.Packets.Game.Room import RoomDetails, RoomChange
from LoreleiLib.Packets.Game.Chat import MessageSent, MessageType
from LoreleiLib.Packets.Game.Map import RegionMapPacket, PlayerPositionPacket
from LoreleiLib.Character import Character


class RoomStateManager:

    Instance = None
    GameServer = GameServer().Instance

    def __init__(self, connection, character):
        self.connection = connection
        self.character = character

    def handle_start(self):
        if self.character.room is None:
            self.character.move(RoomManager().StartingRoom)
        else:
            self.character.move(self.character.room)
        self.sendRoomData()
        self.sendRegionMap(self.character.room.region)

    def handle_data(self, data):
        # Handle movement
        if isinstance(data, RoomChange):
            self.handleRoomChange(data)
        # Handle Attacks
        # Handle Interaction

    def handleRoomChange(self, data):
        # type: (RoomChange) -> None
        # Handle movement
        characterRoom = self.character.room  # type: MapTile
        if data.direction is not None:
            direction = data.direction
            exits = characterRoom.exitDetails.exits # type: dict

            if exits[direction] is not None:
                targetRoom = RoomStateManager.GameServer.WorldManager.RoomManager.getRoom(exits[direction]) # type: MapTile
                self.characterChangeRegions(self.character.room, targetRoom)
                self.characterChangeRooms(self.character.room, targetRoom)

    def characterChangeRegions(self, currentRoom, targetRoom):
        if targetRoom.region != currentRoom.region:
            self.sendRegionMap(targetRoom.region)

    def characterChangeRooms(self, currentRoom, targetRoom):
        if targetRoom is not None:
            for character in targetRoom.players:  # type: Character
                player = RoomStateManager.GameServer.getPlayerByName(character.name)
                if player is not None:
                    player.sendLine(MessageSent(MessageType.Raw, self.character.name + " has entered the room"))
            self.character.move(targetRoom)
            self.sendRoomData()

            for character in currentRoom.players:  # type: Character
                player = RoomStateManager.GameServer.getPlayerByName(character.name)
                if player is not None:
                    player.sendLine(MessageSent(MessageType.Raw, self.character.name + " has left the room"))
            self.character.move(targetRoom)
            self.sendRoomData()

    def sendRoomData(self):
        self.connection.sendLine(RoomDetails(self.character.room))
        self.connection.sendLine(PlayerPositionPacket(self.character.room.uuid))

    def sendRegionMap(self, region):
        self.connection.sendLine(RegionMapPacket(region))