from LoreleiServer.GameServer.GameServer import GameServer
from LoreleiLib.Packets.Game.Chat import MessageSent, MessageRecieved, ChatPacket, MessageType, WhisperSearchPlayer, \
    WhisperSearchPlayerResult
from LoreleiServer.GameServer.Managers.Player import Player


class ChatStateManager:

    Instance = None
    GameServer = GameServer().Instance
    InvalidTypes = [MessageType.Combat, MessageType.System]

    def __init__(self, connection, username):
        self.username = username
        self.connection = connection

    def handle_data(self, data):
        # type: (ChatPacket) -> None
        if isinstance(data, MessageSent):
            if data.type in ChatStateManager.InvalidTypes:
                self.connection.sendLine(MessageRecieved(MessageType.System, "You can't send that type of message"))
            else:
                if data.type == MessageType.Whisper:
                    if data.target.strip() == "":
                        self.connection.sendLine(MessageRecieved(MessageType.System, "You need to enter a person to "
                                                                                     "whisper"))
                    else:
                        target = self.searchPlayerByPartial(data.target)
                        if target is not None:
                            target.sendLine(MessageRecieved(data.type, data.message, self.username))
                        else:
                            self.connection.sendLine(MessageRecieved(MessageType.System, "That user was not found"))
                else:
                    for player in ChatStateManager.GameServer.Players.values():
                        if player is not None:
                            player.sendLine(MessageRecieved(data.type, data.message, self.username))

        if isinstance(data, WhisperSearchPlayer):
            character = self.searchPlayerByPartial(data.searchString).character
            name = ""
            if character is not None:
                name = character.name
            self.connection.sendLine(WhisperSearchPlayerResult(name))

    def searchPlayerByPartial(self, value):
        # type: (str) -> Player or None
        # TODO: Optimize with a Trie
        # https://stackoverflow.com/questions/18066603/fastest-way-to-search-python-dict-with-partial-keyword
        if ChatStateManager.GameServer.Players.has_key(value):
            return ChatStateManager.GameServer.Players[value]

        key = next( (key for key in ChatStateManager.GameServer.Players if key.startswith(value)), None )
        if key is not None:
            return ChatStateManager.GameServer.Players[key]
        return None