from twisted.protocols.basic import LineReceiver
from LoreleiServer.StateManagers.Common import StateManager
from LoreleiServer.StateManagers.CharacterCreation import CharacterCreationManager
from LoreleiServer.StateManagers.Game import GamePlayManager
from LoreleiLib.Packets.Login import LoginAttemptRequest, LoginAttemptResponse, AccountCreateRequest, \
    AccountCreateResponse
from LoreleiLib.Packets.View import ViewType, ViewPacket
from LoreleiServer.Database.Account import AccountSchema, username_exists, user_online, getAccountByUsername, \
    create_account, login
from LoreleiServer.Database.Character import loadCharacter


class LoginManager(StateManager):

    def __init__(self, chat_connection):
        # type: (LineReceiver) -> None
        super(LoginManager, self).__init__(chat_connection)

    def handle_data(self, data):
        # type: (object) -> StateManager or None
        if isinstance(data, LoginAttemptRequest):
            return self.attempt_login(data)
        if isinstance(data, AccountCreateRequest):
            return self.attempt_create(data)
        return None

    def attempt_login(self, attempt):
        # type: (LoginAttemptRequest) -> StateManager
        if login(AccountSchema(attempt.username, attempt.password)):
            self.connection.username = attempt.username
            self.connection.password = attempt.password
            self.connection.sendLine(LoginAttemptResponse(True, "Login Successful"))
            character = loadCharacter(attempt.username)
            if character is None:
                self.connection.sendLine(ViewPacket(ViewType.CharacterCreation))
                return CharacterCreationManager(self.connection)
            else:
                self.connection.sendLine(ViewPacket(ViewType.Game))
                return GamePlayManager(self.connection, character)


        else:
            # Why did login fail
            # User doesn't exist
            if not username_exists(attempt.username):
                return self.connection.sendLine(LoginAttemptResponse(False, "Account doesn't exist"))
            # User is already online
            if user_online(attempt.username):
                return self.connection.sendLine(LoginAttemptResponse(False, "Account already signed in"))
            #
            if getAccountByUsername(attempt.username).password is not attempt.password:
                return self.connection.sendLine(LoginAttemptResponse(False, "Invalid Password"))

    def attempt_create(self, attempt):
        # type: (AccountCreateRequest) -> None
        if create_account(AccountSchema(attempt.username, attempt.password)):
            self.connection.sendLine(AccountCreateResponse(True, "Account Created! Please login"))
        else:
            if len(attempt.username) <= 6:
                return self.connection.sendLine(AccountCreateResponse(False, "That username is too short. (More than 6 characters)"))
            if username_exists(attempt.username):
                return self.connection.sendLine(AccountCreateResponse(False, "That username is taken"))
            if len(attempt.password) <= 6:
                return self.connection.sendLine(AccountCreateResponse(False, "That password is too short. (More than 6 characters)"))
            return self.connection.sendLine(AccountCreateResponse(False, "Something went wrong"))