from LoreleiServer.StateManagers.Common import StateManager
from LoreleiLib.Packets.Common import PlayerQuitPacket
from LoreleiServer.Database.Character import saveCharacter
from LoreleiLib.Packets.Game.Chat import ChatPacket
from LoreleiLib.Packets.Game.Room import RoomPacket
from LoreleiLib.Packets.Game.Character import CharacterPacket, CharacterDetailsPacket
from LoreleiServer.GameServer.GameServer import GameServer
from LoreleiServer.GameServer.Managers.Player import Player
from LoreleiServer.StateManagers.GameManagers.Chat import ChatStateManager
from LoreleiServer.StateManagers.GameManagers.Room import RoomStateManager


class GamePlayManager(StateManager):

    def __init__(self, chat_connection, character):
        super(GamePlayManager, self).__init__(chat_connection)
        self.username = chat_connection.username
        self.password = chat_connection.password
        self.character = character
        self.chatManager = ChatStateManager(self.connection, self.character.name)
        self.roomManager = RoomStateManager(self.connection, self.character)
        GameServer().Instance.playerLoggedIn(Player(character, chat_connection))

    def handle_start(self):
        self.roomManager.handle_start()
        self.connection.sendLine(CharacterDetailsPacket(self.character))

    def handleLogout(self):
        GameServer().Instance.playerLoggedOff(self.character.name)

    def handle_data(self, data):
        if isinstance(data, PlayerQuitPacket):
            saveCharacter(self.character)
        if isinstance(data, ChatPacket):
            self.chatManager.handle_data(data)
        if isinstance(data, RoomPacket):
            self.roomManager.handle_data(data)
