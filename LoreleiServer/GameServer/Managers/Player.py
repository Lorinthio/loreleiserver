from LoreleiLib.Character import Character
from LoreleiLib.Packets.Game.Chat import MessageRecieved, MessageType


class Player:

    def __init__(self, character, connection):
        self.character = character # type: Character
        self.connection = connection

    def sendLine(self, line):
        # type: (object) -> None
        self.connection.sendLine(line)


class PlayerManager(object):
    Players = {}

    def playerLoggedIn(self, playerWhoLogged):
        PlayerManager.Players[playerWhoLogged.character.name] = playerWhoLogged
        print "PlayerManager: {} logged in".format(playerWhoLogged.character.name)

        for player in PlayerManager.Players.values():
            if player is not None:
                player.sendLine(MessageRecieved(MessageType.System, "{} has logged in".format(playerWhoLogged.character.name)))

    def playerLoggedOff(self, name):
        PlayerManager.Players[name] = None
        for player in PlayerManager.Players.values():
            if player is not None:
                player.sendLine(MessageRecieved(MessageType.System, "{} has logged out".format(name)))

    def getPlayerByName(self, name):
        if PlayerManager.Players.has_key(name):
            return PlayerManager.Players[name]
        return None