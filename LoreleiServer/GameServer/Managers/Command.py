class CommandManager(object):

    Instance = None

    def __init__(self):
        if CommandManager.Instance is None:
            self.stopCallback = None
            self.commands = {"stop": self.stop, "shutdown": self.stop, "help": self.help}
            CommandManager.Instance = self

    def handleServerCommand(self, text):
        # type: (str) -> bool
        text = text.strip().lower()

        if self.commands.has_key(text):
            result = self.commands[text]()
            if result is None:
                return True
        else:
            self.help()
            return True

    def help(self):
        print "Available Commands, {}".format(self.commands.keys())

    # Server stopping
    def stop(self):
        self.shutdown()
        if self.stopCallback is not None:
            self.stopCallback()
        return False

    def shutdown(self):
        print "Shutting down..."

    def setStopCallback(self, stopCallback):
        self.stopCallback = stopCallback