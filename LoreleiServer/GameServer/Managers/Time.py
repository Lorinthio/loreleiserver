import time


class TimeManager(object):

    Now = time.time()

    def getDelta(self):
        # type: () -> float
        now = time.time()
        delta = now - TimeManager.Now
        TimeManager.Now = now
        return delta