from LoreleiServer.Data.Character import CharacterRaceManager, CharacterClassManager
from LoreleiServer.Data.Equipment import EquipmentManager
from LoreleiServer.Data.World.World import WorldManager
from LoreleiServer.GameServer.Managers.Player import PlayerManager
from LoreleiServer.GameServer.Managers.Command import CommandManager
from LoreleiServer.GameServer.Managers.Time import TimeManager


class GameServer(PlayerManager, TimeManager):

    CharacterRaceManager = None
    CharacterClassManager = None
    EquipmentManager = None
    WorldManager = None
    CommandManager = None
    Instance = None

    def __init__(self):
        if GameServer.Instance is None:
            if GameServer.CharacterRaceManager is None:
                GameServer.CharacterRaceManager = CharacterRaceManager().Instance
            if GameServer.CharacterClassManager is None:
                GameServer.CharacterClassManager = CharacterClassManager().Instance
            if GameServer.EquipmentManager is None:
                GameServer.EquipmentManager = EquipmentManager().Instance
            if GameServer.WorldManager is None:
                GameServer.WorldManager = WorldManager().Instance
            if GameServer.CommandManager is None:
                GameServer.CommandManager = CommandManager().Instance

            super(GameServer, self).__init__()
            GameServer.Instance = self

    def tick(self):
        delta = self.getDelta()