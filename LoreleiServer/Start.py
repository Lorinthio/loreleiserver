from twisted.internet.protocol import Factory
from twisted.protocols.basic import LineReceiver
from twisted.internet import reactor
from LoreleiServer.StateManagers.Login import LoginManager
from LoreleiServer.StateManagers.CharacterCreation import CharacterCreationManager
from LoreleiServer.StateManagers.Game import GamePlayManager
from Database.Account import AccountSchema, reset_online_status, logout
from LoreleiLib.Packets.Common import Packet, PacketSet, PlayerQuitPacket
from LoreleiLib.Packets.View import ViewPacket, ViewType
import cPickle
from threading import Timer
from datetime import datetime
from twisted.internet.task import LoopingCall
from LoreleiServer.GameServer.GameServer import GameServer
from threading import Thread


class Chat(LineReceiver):

    def __init__(self, addr):
        self.dataObject = ""
        self.addr = addr
        self.lines = []
        self.lastCallTimestamp = datetime.utcnow()
        self.quit = False
        self.lostConnect = False
        self.username = None
        self.password = None
        self.timer = None
        self.stateManager = LoginManager(self)

    def sendLine(self, line):
        self.lastCallTimestamp = datetime.utcnow()
        self.lines.append(line)
        if self.timer is None:
            self.timer = Timer(0.01, self.delaySendLines)
            self.timer.start()

    def delaySendLines(self):
        if len(self.lines) > 1:
            LineReceiver.sendLine(self, PacketSet(self.lines).serialize())
        elif len(self.lines) == 1:
            line = self.lines[0]
            if isinstance(line, Packet):
                LineReceiver.sendLine(self, line.serialize())
            else:
                LineReceiver.sendLine(self, line)
        self.lines = []
        self.timer = None

    def connectionMade(self):
        if self.lostConnect:
            self.sendViewPacket()
        self.lostConnect = False

    def sendViewPacket(self):
        viewType = ViewType.Login
        if isinstance(self.stateManager, LoginManager):
            viewType = ViewType.Login
        elif isinstance(self.stateManager, CharacterCreationManager):
            viewType = ViewType.CharacterCreation
        elif isinstance(self.stateManager, GamePlayManager):
            viewType = ViewType.Game
        self.sendLine(ViewPacket(viewType))

    def connectionLost(self, reason):
        print "Lost Connection"
        self.lostConnect = True
        if self.quit:
            self.stateManager.handleLogout()
            logout(AccountSchema(self.username, self.password))

    def dataReceived(self, data):
        try:
            data = cPickle.loads(data)
        except Exception as err:
            self.dataObject += data
            try:
                data = cPickle.loads(self.dataObject)
                self.dataObject = ""
                # print "Unpickled Large Object"
            except Exception as err2:
                pass
                # print "Object not finished"

        if isinstance(data, PacketSet):
            for packet in data.packets:
                self.handlePacket(packet)
        else:
            self.handlePacket(data)

        if isinstance(data, PlayerQuitPacket):
            self.quit = True

    def handlePacket(self, data):
        newState = self.stateManager.handle_data(data)
        if newState is not None:
            self.stateManager = newState
            self.stateManager.handle_start()


class ChatFactory(Factory):

    def __init__(self):
        self.removalDelay = 30
        self.users = {} # maps ips to instances
        self.start()

    def start(self):
        reset_online_status()
        Timer(5.0, self.clearOldConnections).start()

    def clearOldConnections(self):
        Timer(5.0, self.clearOldConnections).start()

        removed = 0
        d = dict(self.users)
        for key, value in d.iteritems():
            if value.quit:
                del(self.users[key])
                removed += 1
            else:
                if value.lostConnect:
                    difference = datetime.utcnow() - value.lastCallTimestamp
                    delta = datetime.strptime(str(difference), '%H:%M:%S.%f')
                    if delta.second > self.removalDelay:
                        chatInstance = self.users[key]
                        logout(AccountSchema(chatInstance.username, chatInstance.password))
                        del(self.users[key])
                        removed += 1

    def buildProtocol(self, addr):
        chat = None
        if self.users.has_key(addr.host):
            chat = self.users[addr.host]
            if not chat.quit:
                chat.lastCallTimestamp = datetime.utcnow()
                chat.lostConnect = False
                chat.sendViewPacket()
            else:
                chat = Chat(addr)
        else:
            chat = Chat(addr)
        self.users[addr.host] = chat
        return chat


def stop():
    reactor.stop()

gameServer = GameServer()
def main():
    tick = LoopingCall(gameServer.tick)
    GameServer().CommandManager.Instance.setStopCallback(stop)
    tick.start(1 / 20.0)

    Thread(target=recieveInput).start()

    reactor.listenTCP(8123, ChatFactory())
    reactor.run()


def recieveInput():
    running = True
    while running:
        running = GameServer().CommandManager.Instance.handleServerCommand(raw_input(">"))


if __name__ == "__main__":
    main()