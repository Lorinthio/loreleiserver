from LoreleiLib.Map import MapRegion
from uuid import uuid4


class RegionManager:

    RegionIds = {}
    RegionNames = {}
    Instance = None # type: RegionManager

    def __init__(self):
        if RegionManager.Instance is None:
            RegionManager.Instance = self
            self.load()

    def load(self):
        self.addRegion(MapRegion(uuid4(), 'Ruined Halls', "A small dungeon hidden in the hills that is home to some "
                                                          "interesting stonework and some creepy rooms"))
        print "Loaded {} regions".format(len(RegionManager.RegionIds))

    def addRegion(self, region):
        # type: (MapRegion) -> None
        RegionManager.RegionIds[region.uuid] = region
        RegionManager.RegionNames[region.name] = region

    def getRegionById(self, uuid):
        # type: (uuid4) -> MapRegion
        if RegionManager.RegionIds.has_key(uuid):
            return RegionManager.RegionIds[uuid]
        return None

    def getRegionByName(self, name):
        # type: (str) -> MapRegion
        if RegionManager.RegionNames.has_key(name):
            return RegionManager.RegionNames[name]
        return None

    def finalize(self):
        for region in RegionManager.RegionIds.values(): # type: MapRegion
            region.makeMapData()
