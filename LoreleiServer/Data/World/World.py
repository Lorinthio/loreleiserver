from LoreleiServer.Data.World.Region import RegionManager
from LoreleiServer.Data.World.Room import RoomManager


class WorldManager:

    RegionManager = None
    RoomManager = None
    Instance = None

    def __init__(self):
        if WorldManager.Instance is None:
            WorldManager.Instance = self
            self.load()

    def load(self):
        WorldManager.RegionManager = RegionManager().Instance
        WorldManager.RoomManager = RoomManager().Instance
        WorldManager.RegionManager.finalize()