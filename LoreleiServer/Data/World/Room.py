from LoreleiLib.Map import MapTile, MapDirection, MapExitDescriptions
from uuid import uuid4
from LoreleiServer.Data.World.Region import RegionManager


class RoomManager:

    StartingRoom = None # type: MapTile
    Rooms = {} # type: dict
    Instance = None

    def __init__(self):
        if RoomManager.Instance is None:
            RoomManager.Instance = self
            self.load()

            self.startRoomId = None

    def load(self):
        self.makeTestArea()
        print "Loaded {} rooms".format(len(RoomManager.Rooms))

    def makeTestArea(self):
        ids = [uuid4(), uuid4(), uuid4(), uuid4(), uuid4()]
        region = RegionManager().Instance.getRegionByName('Ruined Halls')
        self.startRoomId = ids[0]
        self.addRoom(MapTile(ids[0], 0, 0, region, "A massive overgrown boulder in a dire mountain range marks the"
                                                   " entrance to this dungeon. \nBeyond the overgrown boulder lies a"
                                                   " narrow, timeworn room. It's covered in roots, remains and "
                                                   "moss. Your torch allows you to see an altar, forgotten and "
                                                   "desolated by time itself.",
                             MapExitDescriptions(northId=ids[1], northDescription="Into the ruins")))

        self.addRoom(
            MapTile(ids[1], 0, 1, region, "You are now closer to the altar, it is covered in dust and what looks like "
                                          "gold pieces. The exit of the dungeon is south of you leading outdoors."
                                          " You feel a gentle breeze coming from the eastern end of the "
                                          "altar room..."
                                          "You have found what seems to be a small passage, though large enough to"
                                          "crawl through",
                    MapExitDescriptions(southId=ids[0], southDescription="Back outside", eastId=ids[2],
                                        eastDescription="Crawl through the passage")))

        self.addRoom(
            MapTile(ids[2], 1, 1, region, "The room has opened up into a musky but spacious foyer. Desolate ruined "
                                          "columns stretch up the sides of the room outlining what must've been a "
                                          "pretty grand entrance in its golden days. For now it is simply a muted"
                                          "presence of its old self.\n"
                                          "A stairway leads upwards to the south, and another down and deeper to the north",
                    MapExitDescriptions(westId=ids[1], westDescription="To the Altar", northId=ids[4],
                                        northDescription="Downstairs", southId=ids[3],
                                        southDescription="Upstairs")))

        self.addRoom(
            MapTile(ids[3], 1, 0, region, "The stretch of stairs has taken you to what seems to be a cove that "
                                          "overlooks the dungeon entrance. You hadn't noticed it previously but if"
                                          "there were archers watching you... you could've been killed without the"
                                          "slightest warning. \n"
                                          "You could hop down from here, but there would be no getting back up",
                    MapExitDescriptions(northId=ids[2], northDescription="Downstairs to the Foyer",
                                        westId=ids[0], westDescription="Jump down"), z= 1))

        self.addRoom(
            MapTile(ids[4], 1, 2, region, "You have travelled deeper into the dungeon, it looks like this could've"
                                          "been a prison previously, now is just a battered passage way with broken "
                                          "iron bars and locks litering the ground.",
                    MapExitDescriptions(southId=ids[2], southDescription="Upstairs to the Foyer"), z= -1))

        RoomManager.StartingRoom = self.getRoom(self.startRoomId)

    def addRoom(self, room):
        # type: (MapTile) -> None
        RoomManager.Rooms[room.uuid] = room

    def getRoom(self, uuid):
        return RoomManager.Rooms[uuid]