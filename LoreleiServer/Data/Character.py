from LoreleiLib.Character import CharacterClass, CharacterRace
from LoreleiServer.Data.Equipment import EquipmentManager
from LoreleiLib.Statistics import AttributeSet
from LoreleiLib.Objects import WeaponType, ArmorType, EquipmentSlot, Armor, Weapon, Shield, EquipmentItem


class CharacterRaceManager(object):

    Instance = None
    Races = []

    def __init__(self):
        # type: () -> None
        if CharacterRaceManager.Instance is None:
            self.load()
            CharacterRaceManager.Instance = self

    def load(self):
        # type: () -> None
        self.addRace(CharacterRace('Human', 'An overall completely ordinary race, but generally well liked',
                                    AttributeSet(Charisma=2)))

        self.addRace(CharacterRace('Elf', 'Folk who generally keep to themselves. They are very keen and intelligent, '
                                           'but slightly frail',
                                    AttributeSet(Strength=-1, Constitution=-1, Dexterity=2, Intelligence=2)))

        self.addRace(CharacterRace('Dwarf', 'Stocky folk who enjoy a good brew and a good time but have rather thick skulls',
                                    AttributeSet(Strength=1, Constitution=3, Agility=-1, Intelligence=-1)))

        self.addRace(CharacterRace('Drow', 'Creatures of the night who are quick on their feet and witty',
                                    AttributeSet(Constitution=-1, Agility=2, Intelligence=2, Charisma=-1)))

        self.addRace(CharacterRace('Half-Orc', 'Brutish creatures who believe in raw strength over politeness or book smarts',
                                    AttributeSet(Strength=3, Constitution=3, Wisdom=-1, Intelligence=-1, Charisma=-2)))

        self.addRace(CharacterRace('Aquos', 'Mystic creatures who reside in wet/aquatic environments. They aren\'t the fastest creatures, but are very intelligent and wise',
                                    AttributeSet(Dexterity=-1, Agility=-1, Wisdom=2, Intelligence=2)))

        print("Loaded {} races".format(len(CharacterRaceManager.Races)))

    def addRace(self, race):
        # type: (CharacterRace) -> None
        CharacterRaceManager.Races.append(race)

    def getRace(self, raceName):
        for race in CharacterRaceManager.Races:  # type: CharacterRace
            if race.name == raceName:
                return race


class CharacterClassManager(object):

    Instance = None
    Classes = []

    def __init__(self):
        # type: () -> None
        if CharacterClassManager.Instance is None:
            self.load()
            CharacterClassManager.Instance = self

    def load(self):
        # type: () -> None
        self.addClass(CharacterClass('Fighter', 'A melee class that can either focus on damage or defending their allies',
                                     {},
                                     [WeaponType.Sword, WeaponType.Axe, WeaponType.Mace,
                                      WeaponType.Shield,
                                      WeaponType.Two_Handed_Axe, WeaponType.Two_Handed_Mace,
                                      WeaponType.Two_Handed_Sword],
                                     [ArmorType.Leather, ArmorType.Chain],
                                     ["Shortsword", "Heater Shield", "Rusted Chainmail Coif", "Rusted Chainmail Shirt",
                                      "Rusted Chainmail Gloves", "Rusted Chainmail Leggings", "Rusted Chainmail Boots"],
                                     AttributeSet(Strength=14, Constitution=14, Dexterity=12, Agility=12,
                                                  Intelligence=8, Wisdom=10, Charisma=10)))

        self.addClass(CharacterClass('Rogue', 'A class that focuses on speed and agility and reflexes to avoid damage and deal as much damage as possible',
                                     {},
                                     [WeaponType.Dagger, WeaponType.Sword, WeaponType.Bow],
                                     [ArmorType.Leather],
                                     ["Dirk", "Dirk", "Tanned Leather Cap", "Tanned Leather Vest",
                                      "Tanned Leather Gloves", "Tanned Leather Pants", "Tanned Leather Boots"],
                                     AttributeSet(Strength=12, Constitution=10, Dexterity=14, Agility=14,
                                                  Intelligence=10, Wisdom=10, Charisma=10)))

        self.addClass(CharacterClass('Mage', 'A magical class that casts offensive and supportive spells',
                                     {},
                                     [WeaponType.Dagger, WeaponType.Staff],
                                     [ArmorType.Cloth],
                                     ["Rowan Crook", "Bear Cloth Cap", "Bear Cloth Coat",
                                      "Bear Cloth Gloves", "Bear Cloth Chaps", "Bear Cloth Moccasins"],
                                     AttributeSet(Strength=8, Constitution=10, Dexterity=12, Agility=12,
                                                  Intelligence=14, Wisdom=14, Charisma=10)))

        print("Loaded {} classes".format(len(CharacterClassManager.Classes)))

    def addClass(self, characterClass):
        # type: (CharacterClass) -> None
        CharacterClassManager.Classes.append(characterClass)

        equipment = {}
        for itemname in characterClass.starterSet: # type: str
            item = EquipmentManager().Instance.lookupItem(itemname) # type: EquipmentItem
            if isinstance(item, Shield):
                equipment[item.equipmentSlot] = item
            if isinstance(item, Weapon):
                if equipment.has_key(item.equipmentSlot) and item.equipmentSlot is EquipmentSlot.Main_Hand:
                    if item.weaponType.value < WeaponType.OneHanded.value: # If is one handed
                        equipment[EquipmentSlot.Off_Hand] = item
                else:
                    equipment[item.equipmentSlot] = item
            if isinstance(item, Armor):
                equipment[item.equipmentSlot] = item
        characterClass.starterEquipment = equipment

    def getClass(self, className):
        for characterClass in CharacterClassManager.Classes:  # type: CharacterClass
            if characterClass.name == className:
                return characterClass

    def getItem(self, name):
        EquipmentManager.Instance.lookupItem(name)