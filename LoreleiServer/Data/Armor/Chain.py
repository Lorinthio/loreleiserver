from LoreleiLib.Objects import Armor, ArmorType, EquipmentSlot
from LoreleiLib.Statistics import AttributeSet
from LoreleiServer.Data.Armor.Common import ArmorTypeManager


class ChainManager(ArmorTypeManager):
    ChainArmor = []
    ChainArmorDict = {}

    def __init__(self):
        super(ChainManager, self).__init__(ArmorType.Chain, ChainManager.ChainArmor, ChainManager.ChainArmorDict)
        if len(ChainManager.ChainArmor) == 0:
            self.load()

    def load(self):
        # Name, Description, Level, Value, Weight, Damage, Speed, weaponType
        self.loadRustedChainmail()

    def loadRustedChainmail(self):
        self.add(Armor("Rusted Chainmail Coif",
                       "The chains get stuck in your hair and leave a brown rusty tint",
                       1, 0, 1.2, EquipmentSlot.Head, self.armorType,
                       AttributeSet(Defense=2, MagicDefense=1)))
        self.add(Armor("Rusted Chainmail Shirt",
                       "A left over shirt from a garrison, quite rusty",
                       1, 0, 2.5, EquipmentSlot.Chest, self.armorType,
                       AttributeSet(Defense=4, MagicDefense=1)))
        self.add(Armor("Rusted Chainmail Leggings",
                       "The rust is everywhere, but at least it can stop a blade",
                       1, 0, 2.2, EquipmentSlot.Legs, self.armorType,
                       AttributeSet(Defense=2, MagicDefense=1)))
        self.add(Armor("Rusted Chainmail Boots",
                       "Rusty boots, they aren't very comfortable",
                       1, 0, 2.0, EquipmentSlot.Feet, self.armorType,
                       AttributeSet(Defense=1, MagicDefense=1)))
        self.add(Armor("Rusted Chainmail Gloves",
                       "Gloves made of chainmail that has rusted metal coming off",
                       1, 0, 1, EquipmentSlot.Hands, self.armorType,
                       AttributeSet(Defense=1, MagicDefense=0)))