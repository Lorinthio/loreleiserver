from LoreleiLib.Objects import Armor, ArmorType
from random import randint


class ArmorTypeManager(object):

    def __init__(self, armorType, staticList, staticDict):
        # type: (ArmorType, list, dict) -> None
        self.armorType = armorType
        self.list = staticList
        self.lookupDict = staticDict

    def add(self, armor):
        # type: (Armor) -> None
        if armor.armorType != self.armorType:
            print armor.name + " does not match armor type, " + self.armorType.name
            return
        if not self.lookupDict.has_key(armor.name):
            self.list.append(armor)
            self.lookupDict[armor.name] = armor

    def getRandomItem(self):
        # type: () -> Armor
        if len(self.list) > 0:
            return self.list[randint(0, len(self.list)-1)]
        return None

    def lookupItem(self, name):
        # type: (str) -> Weapon
        return self.lookupDict.get(name, None)