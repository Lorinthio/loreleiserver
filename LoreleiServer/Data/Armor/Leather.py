from LoreleiLib.Objects import Armor, ArmorType, EquipmentSlot
from LoreleiLib.Statistics import AttributeSet
from LoreleiServer.Data.Armor.Common import ArmorTypeManager


class LeatherManager(ArmorTypeManager):
    LeatherArmor = []
    LeatherArmorDict = {}

    def __init__(self):
        super(LeatherManager, self).__init__(ArmorType.Leather, LeatherManager.LeatherArmor, LeatherManager.LeatherArmorDict)
        if len(LeatherManager.LeatherArmor) == 0:
            self.load()

    def load(self):
        # Name, Description, Level, Value, Weight, Damage, Speed, weaponType
        self.loadTannedLeather()

    def loadTannedLeather(self):
        self.add(Armor("Tanned Leather Cap",
                       "This tanned leather cap is light brown but firm",
                       1, 0, 1.2, EquipmentSlot.Head, self.armorType,
                       AttributeSet(Defense=2, MagicDefense=2)))
        self.add(Armor("Tanned Leather Vest",
                       "This vest is sturdy enough to... possibly stop a blade?",
                       1, 0, 2.5, EquipmentSlot.Chest, self.armorType,
                       AttributeSet(Defense=2, MagicDefense=2)))
        self.add(Armor("Tanned Leather Pants",
                       "These are pretty regular leather pants, just not black and tight",
                       1, 0, 2.2, EquipmentSlot.Legs, self.armorType,
                       AttributeSet(Defense=1, MagicDefense=1)))
        self.add(Armor("Tanned Leather Boots",
                       "These leather boots are pretty comfortable",
                       1, 0, 2.0, EquipmentSlot.Feet, self.armorType,
                       AttributeSet(Defense=1, MagicDefense=2)))
        self.add(Armor("Tanned Leather Gloves",
                       "Leather gloves with cut off tips for maximum dexterity",
                       1, 0, 1, EquipmentSlot.Hands, self.armorType,
                       AttributeSet(Defense=1, MagicDefense=1)))