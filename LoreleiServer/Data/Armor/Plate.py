from LoreleiLib.Objects import Armor, ArmorType, EquipmentSlot
from LoreleiLib.Statistics import AttributeSet
from LoreleiServer.Data.Armor.Common import ArmorTypeManager


class PlateManager(ArmorTypeManager):
    PlateArmor = []
    PlateArmorDict = {}

    def __init__(self):
        super(PlateManager, self).__init__(ArmorType.Plate, PlateManager.PlateArmor, PlateManager.PlateArmorDict)
        if len(PlateManager.PlateArmor) == 0:
            self.load()

    def load(self):
        # Name, Description, Level, Value, Weight, Damage, Speed, weaponType
        pass