from LoreleiLib.Objects import Armor, ArmorType, EquipmentSlot
from LoreleiLib.Statistics import AttributeSet
from LoreleiServer.Data.Armor.Common import ArmorTypeManager


class ClothManager(ArmorTypeManager):
    ClothArmor = []
    ClothArmorDict = {}

    def __init__(self):
        super(ClothManager, self).__init__(ArmorType.Cloth, ClothManager.ClothArmor, ClothManager.ClothArmorDict)
        if len(ClothManager.ClothArmor) == 0:
            self.load()

    def load(self):
        # Name, Description, Level, Value, Weight, Damage, Speed, weaponType
        self.loadBearCloth()

    def loadBearCloth(self):
        self.add(Armor("Bear Cloth Cap",
                       "A small hat made of soft bear skin",
                       1, 0, 0.2, EquipmentSlot.Head, self.armorType,
                       AttributeSet(Defense=1, MagicDefense=2)))
        self.add(Armor("Bear Cloth Coat",
                       "A soft coat made of bear skin",
                       1, 0, 1.4, EquipmentSlot.Chest, self.armorType,
                       AttributeSet(Defense=1, MagicDefense=4)))
        self.add(Armor("Bear Cloth Chaps",
                       "Ass chaps made of soft bear skin",
                       1, 0, 1.0, EquipmentSlot.Legs, self.armorType,
                       AttributeSet(Defense=1, MagicDefense=2)))
        self.add(Armor("Bear Cloth Moccasins",
                       "Very comfortable shoots made of soft bear skin",
                       1, 0, 0.8, EquipmentSlot.Feet, self.armorType,
                       AttributeSet(Defense=1, MagicDefense=1)))
        self.add(Armor("Bear Cloth Gloves",
                       "Keep your hands warm with these gloves made of soft bear skin!",
                       1, 0, 0.5, EquipmentSlot.Hands, self.armorType,
                       AttributeSet(Defense=0, MagicDefense=1)))