from LoreleiLib.Objects import Weapon, WeaponType
from LoreleiServer.Data.Weapons.Common import WeaponTypeManager


class SwordManager(WeaponTypeManager):

    Swords = []
    SwordDict = {}

    def __init__(self):
        super(SwordManager, self).__init__(WeaponType.Sword, SwordManager.Swords, SwordManager.SwordDict)
        if len(SwordManager.Swords) == 0:
            self.load()

    def load(self):
        # Name, Description, Level, Value, Weight, Damage, Speed, Sword
        # One Handed
        self.add(
            Weapon('Shortsword',
                  'The most basic sword, wieldable by anyone with one hand with more than three fingers.',
                  1, 0, 2.5,
                  '1d8',
                  2.4, self.weaponType))
        self.add(
            Weapon('Broadsword',
                  "A sword most can wield, though few can with the aptitude to outweigh its advanced weight compared to smaller swords.",
                  1, 0, 3,
                  '1d10',
                  2.8, self.weaponType))
        self.add(
            Weapon('Katana',
                  "A blade not known for its durability, but rather its ability to cause damage far after the initial strike.",
                  1, 0, 2.8,
                  '1d8',
                  2.6, self.weaponType))
        self.add(
            Weapon('Sabre',
                  "A long, thin, single-edged sword. Useful for cutting, slashing, and stabbing.",
                  1, 0, 2.8,
                  '1d8 +1',
                  2.7, self.weaponType))
        self.add(
            Weapon('Falchion',
                  "Your average single-handed sword, made for swift, consecutive strikes to the same location.",
                  1, 0, 2.7,
                  '1d8',
                  2.2, self.weaponType))
        self.add(
            Weapon('Cutlass',
                  "A single-edged weapon with a curved blade. It is an efficient tool for slicing your enemies.",
                  1, 0, 2.9,
                  '1d8 +2',
                  2.8, self.weaponType))