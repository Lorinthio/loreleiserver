from LoreleiLib.Objects import Weapon, WeaponType
from LoreleiServer.Data.Weapons.Common import WeaponTypeManager


class MaceManager(WeaponTypeManager):
    Maces = []
    MaceDict = {}

    def __init__(self):
        super(MaceManager, self).__init__(WeaponType.Mace, MaceManager.Maces, MaceManager.MaceDict)
        if len(MaceManager.Maces) == 0:
            self.load()

    def load(self):
        # Name, Description, Level, Value, Weight, Damage, Speed, weaponType
        self.add(
            Weapon("Flail",
                   "A spiked ball on a chain. It can be used quickly and efficiently by those adept in its functionality.",
                   1, 0, 2.5,
                   "1d6",
                   1.5
                   , self.weaponType)
        )
        self.add(
            Weapon("Gada",
                   "A spherical mace with a long handle. Its unbalanced nature can be utilized by centripetal force, dealing decent damage.",
                   1, 0, 3.5,
                   "1d10 +2",
                   3.5
                   , self.weaponType)
        )
        self.add(
            Weapon("Bludgeon",
                   "A crude weapon meant to deal damage via blunt force trauma.",
                   1, 0, 2.6,
                   "1d8",
                   2.4
                   , self.weaponType)
        )