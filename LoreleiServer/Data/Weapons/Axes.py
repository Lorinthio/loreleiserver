from LoreleiLib.Objects import Weapon, WeaponType
from LoreleiServer.Data.Weapons.Common import WeaponTypeManager


class AxeManager(WeaponTypeManager):
    Axes = []
    AxeDict = {}

    def __init__(self):
        super(AxeManager, self).__init__(WeaponType.Axe, AxeManager.Axes, AxeManager.AxeDict)
        if len(AxeManager.Axes) == 0:
            self.load()

    def load(self):
        # Name, Description, Level, Value, Weight, Damage, Speed, weaponType
        self.add(
            Weapon("Hatchet",
                   "Not much can be said about this hatchet. It has a multitude of uses, one of the lesser being combat.",
                   1, 0, 2.5,
                   "1d6 +1",
                   2.5, self.weaponType)
        )
        self.add(
            Weapon("Tomahawk",
                   "Your average throwing axe.",
                   1, 0, 1.5,
                   "2d4",
                   3, self.weaponType)
        )
        self.add(
            Weapon("Battleaxe",
                   "No larger than your typical axe, it has a single sharp edge and was built to last.",
                   1, 0, 3.1,
                   "1d10 +1",
                   2.9, self.weaponType)
        )