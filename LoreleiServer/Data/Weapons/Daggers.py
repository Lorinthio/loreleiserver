from LoreleiLib.Objects import Weapon, WeaponType
from LoreleiServer.Data.Weapons.Common import WeaponTypeManager


class DaggerManager(WeaponTypeManager):
    Daggers = []
    DaggerDict = {}

    def __init__(self):
        super(DaggerManager, self).__init__(WeaponType.Dagger, DaggerManager.Daggers, DaggerManager.DaggerDict)
        if len(DaggerManager.Daggers) == 0:
            self.load()

    def load(self):
        #Name, Description, Level, Value, Weight, Damage, Speed, Dagger
        self.add(
            Weapon('Dirk',
                   'Your typical short, weak dagger.',
                   1, 0, 1.3,
                   '1d6 -1',
                    1.5, self.weaponType))
        self.add(
            Weapon('Kukri',
                   'A practical combat weapon.',
                   1, 0, 1.8,
                   '1d6',
                   1.7, self.weaponType))
        self.add(
            Weapon('Cinquedea',
                   "Heaver than most daggers, it's able to deal an amount of damage that can rival that of a sword.",
                   1, 0, 2.2,
                   '1d6 +1',
                   2.2, self.weaponType))
        self.add(
            Weapon('Baselard',
                   "A dagger, with its most notable quality being abnormality.",
                   1, 0, 2, '(1d6) *2-3',
                   1.7, self.weaponType))
        self.add(
            Weapon('Throwing Knife',
                   "A knife meant for quick, successive throws.",
                   1, 0, 1.2, '2d4 -3',
                   1.2, self.weaponType))