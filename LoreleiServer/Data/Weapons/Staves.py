from LoreleiLib.Objects import Weapon, WeaponType
from LoreleiServer.Data.Weapons.Common import WeaponTypeManager
from LoreleiLib.Statistics import AttributeSet


class StavesManager(WeaponTypeManager):
    Staves = []
    StavesDict = {}

    def __init__(self):
        super(StavesManager, self).__init__(WeaponType.Staff, StavesManager.Staves, StavesManager.StavesDict)
        if len(StavesManager.Staves) == 0:
            self.load()

    def load(self):
        # Name, Description, Level, Value, Weight, Damage, Speed
        self.add(
            Weapon("Rowan Crook",
                   "A small wand which has a bit more weight, you could probably hit something with this",
                   1, 0, 2.8,
                   "1d5",
                   2.3, self.weaponType,
                   AttributeSet(MagicAttack=8))
        )
        self.add(
            Weapon("Elm Crook",
                   "This wand was made from an old mystic tree, you can feel magical power coming from it",
                   1, 0, 2.5,
                   "1d4",
                   2.2, self.weaponType,
                   AttributeSet(MagicAttack=9))
        )
        self.add(
            Weapon("Rowan Staff",
                   "This staff is almost as tall as you, there are some etchings in the dense wood",
                   1, 0, 2.8,
                   "1d7",
                   2.6, self.weaponType,
                   AttributeSet(MagicAttack=10))
        )
        self.add(
            Weapon("Elm Staff",
                   "A staff which contains a gem at its peak. You can practically see the magic turning inside",
                   1, 0, 2.8,
                   "1d6",
                   2.4, self.weaponType,
                   AttributeSet(MagicAttack=12)
                   )
        )