from LoreleiLib.Objects import Weapon, WeaponType
from random import randint


class WeaponTypeManager(object):

    def __init__(self, weaponType, staticList, staticDict):
        # type: (WeaponType, list, dict) -> None
        self.weaponType = weaponType
        self.list = staticList
        self.lookupDict = staticDict

    def add(self, weapon):
        # type: (Weapon) -> None
        if weapon.weaponType != self.weaponType:
            print weapon.name + " does not match weapon type, " + self.weaponType.name
            return
        if not self.lookupDict.has_key(weapon.name):
            self.list.append(weapon)
            self.lookupDict[weapon.name] = weapon

    def getRandomItem(self):
        # type: () -> Weapon
        if len(self.list) > 0:
            return self.list[randint(0, len(self.list)-1)]
        return None

    def lookupItem(self, name):
        # type: (str) -> Weapon
        return self.lookupDict.get(name, None)