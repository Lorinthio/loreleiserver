from LoreleiLib.Objects import WeaponType, Shield
from LoreleiLib.Statistics import AttributeSet
from LoreleiServer.Data.Weapons.Common import WeaponTypeManager


class ShieldManager(WeaponTypeManager):
    Shields = []
    ShieldDict = {}

    def __init__(self):
        super(ShieldManager, self).__init__(WeaponType.Shield, ShieldManager.Shields, ShieldManager.ShieldDict)
        if len(ShieldManager.Shields) == 0:
            self.load()

    def load(self):
        # Name, Description, Level, Value, Weight, Damage, Speed
        self.add(
            Shield("Kite Shield",
                   "A large, rounded, enarmed shield. Provides cover for most of the body.",
                   1, 0, 3.5,
                   "",
                   0.4,
                   AttributeSet(Defense=5))
        )
        self.add(
            Shield("Buckler",
                   "A small shield held by one's fist. Its size renders it ineffective against projectile weapons, but it allows easy deflection of melee weapons.",
                   1, 0, 3.3,
                   "",
                   0.2,
                   AttributeSet(Defense=3))
        )
        self.add(
            Shield("Heater Shield",
                   "Developed as a smaller kite shield, it makes use of a bouche to slide one's sword through.",
                   1, 0, 3.5,
                   "",
                   0.3,
                   AttributeSet(Defense=4))
        )
        self.add(
            Shield("Targe",
                   "A small, round shield with leather strips for handles.",
                   1, 0, 3.3,
                   "",
                   0,
                   AttributeSet(Defense=3))
        )
        self.add(
            Shield("Pavise Shield",
                   "A large convex shield meant to cover one's entire front side.",
                   1, 0, 3.8,
                   "",
                   0.8,
                   AttributeSet(Defense=8))
        )