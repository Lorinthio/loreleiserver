from LoreleiServer.Data.Weapons.Swords import SwordManager
from LoreleiServer.Data.Weapons.Daggers import DaggerManager
from LoreleiServer.Data.Weapons.Axes import AxeManager
from LoreleiServer.Data.Weapons.Maces import MaceManager
from LoreleiServer.Data.Weapons.Shields import ShieldManager
from LoreleiServer.Data.Weapons.Staves import StavesManager
from LoreleiServer.Data.Weapons.Common import WeaponTypeManager
from LoreleiServer.Data.Armor.Common import ArmorTypeManager
from LoreleiServer.Data.Armor.Cloth import ClothManager
from LoreleiServer.Data.Armor.Leather import LeatherManager
from LoreleiServer.Data.Armor.Chain import ChainManager
from LoreleiServer.Data.Armor.Plate import PlateManager
from LoreleiLib.Objects import Weapon, Armor, Item
from random import randint


class EquipmentManager:

    WeaponManager = None
    ArmorManager = None
    Instance = None

    def __init__(self):
        if EquipmentManager.Instance is None:
            EquipmentManager.Instance = self
            EquipmentManager.WeaponManager = WeaponManager()
            EquipmentManager.ArmorManager = ArmorManager()

    def lookupItem(self, name):
        # type: (str) -> Item
        item = EquipmentManager.WeaponManager.lookupItem(name)
        if item is not None:
            return item
        item = EquipmentManager.ArmorManager.lookupItem(name)
        if item is not None:
            return item

    def getRandomItem(self):
        # type: () -> Item
        return [EquipmentManager.WeaponManager, EquipmentManager.ArmorManager][randint(0, 1)].getRandomItem()


class WeaponManager:

    WeaponManagers = []
    Instance = None

    def __init__(self):
        if WeaponManager.Instance is None:
            WeaponManager.Instance = self
            self.load()

    def load(self):
        self.loadManager(SwordManager())
        self.loadManager(DaggerManager())
        self.loadManager(AxeManager())
        self.loadManager(MaceManager())
        self.loadManager(ShieldManager())
        self.loadManager(StavesManager())

    def loadManager(self, manager):
        # type: (WeaponTypeManager) -> None
        WeaponManager.WeaponManagers.append(manager)

    def lookupItem(self, name):
        # type: (str) -> Weapon
        for manager in WeaponManager.WeaponManagers:
            weapon = manager.lookupItem(name)
            if weapon is not None:
                return weapon
        return None

    def getRandomItem(self):
        if len(WeaponManager.WeaponManagers) > 0:
            weapon = WeaponManager.WeaponManagers[randint(0, len(WeaponManager.WeaponManagers)-1)].getRandomItem()
            return weapon
        return None


class ArmorManager:

    ArmorManagers = []
    Instance = None

    def __init__(self):
        if ArmorManager.Instance is None:
            ArmorManager.Instance = self
            self.load()

    def load(self):
        self.loadManager(ClothManager())
        self.loadManager(LeatherManager())
        self.loadManager(ChainManager())
        #self.loadManager(PlateManager())

    def loadManager(self, manager):
        # type: (ArmorTypeManager) -> None
        ArmorManager.ArmorManagers.append(manager)

    def lookupItem(self, name):
        # type: (str) -> Armor
        for manager in ArmorManager.ArmorManagers:
            armor = manager.lookupItem(name)
            if armor is not None:
                return armor
        return None

    def getRandomItem(self):
        if len(ArmorManager.ArmorManagers) > 0:
            armor = ArmorManager.ArmorManagers[randint(0, len(ArmorManager.ArmorManagers)-1)].getRandomItem()
            return armor
        return None


def main():
    manager = EquipmentManager()
    for i in range(0, 100):
        print manager.getRandomItem().name

if __name__ == "__main__":
    main()