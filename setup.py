from setuptools import setup, find_packages
from twisted.internet.task import LoopingCall


setup(name='LoreleiServer',
    version='0.1.8.5', # Don't forget to make a changelog entry in README.rst
    description='The text based MUD made by Lorinthio',
    author='Lorinthio',
    author_email='benrandallswg@gmail.com',
    packages=find_packages(),
    install_requires=[
        'twisted',
        'dataset',
        'LoreleiLib'
    ],
    zip_safe=False)